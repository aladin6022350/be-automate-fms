import mysql from "mysql2";

/**
 * Inserts data into a specified table in the database.
 *
 * @param {object} connectionInfo - The connection information for the database.
 * @param {string} tableName - The name of the table where the data will be inserted.
 * @param {object} data - The data to be inserted into the table.
 * @return {void}
 */

export const insertToDB = (connectionInfo, tableName, data) => {
  const connection = mysql.createConnection(connectionInfo);

  connection.connect((err) => {
    if (err) {
      console.error("Error connecting to database:", err);
      return;
    }

    console.log("Connected to DB");

    const query = `INSERT INTO ${tableName} SET ?`;

    connection.query(query, data, (err) => {
      if (err) {
        console.error("Error inserting data:", err);
        return;
      }
      console.log("Data inserted successfully");

      connection.end((err) => {
        if (err) {
          console.error("Error disconnecting from database:", err);
          return;
        }
        console.log("Disconnected from DB");
      });
    });
  });
};
