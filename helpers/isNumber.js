/**
 * Checks if the given value is a number.
 *
 * @param {any} value - The value to be checked.
 * @return {boolean} Returns true if the value is a number, false otherwise.
 */

export const isNumber = (value) => {
  return /^[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?$/.test(value.trim());
};
