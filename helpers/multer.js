import multer from "multer";
import * as path from "path";

const configStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "cypress/fixtures/configs"); // Change the destination directory to 'cypress/data' for XLSX files
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname); // Keep the original filename
  },
});

// Configure multer to store uploaded files in a directory
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "cypress/data"); // Change the destination directory to 'cypress/data' for XLSX files
  },

  filename: (req, file, cb) => {
    cb(null, file.originalname); // Keep the original filename
  },
});

const fileFilter = (req, file, cb) => {
  // Check if the file has a valid XLSX file extension (e.g., .xlsx)
  const fileExtension = path.extname(file.originalname).toLowerCase();
  if (fileExtension === ".xlsx") {
    cb(null, true); // Accept the file
  } else {
    cb(new Error("Invalid file format. Only XLSX files are allowed."));
  }
};

const olibsFileFilter = (req, file, cb) => {
  if (
    file.mimetype ===
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  ) {
    cb(null, true);
  } else {
    cb(new Error("Invalid file format. Only XLSX files are allowed."));
  }
};

const configFilter = (req, file, cb) => {
  // Check if the file has a valid XLSX file extension (e.g., .xlsx)
  const fileExtension = path.extname(file.originalname).toLowerCase();
  if (fileExtension === ".json") {
    cb(null, true); // Accept the file
  } else {
    cb(new Error("Invalid file format. Only json file are allowed."));
  }
};

export const configUpload = multer({
  storage: configStorage,
  fileFilter: configFilter,
});

export const genericUpload = multer({
  storage: storage,
  fileFilter: fileFilter,
});
export const olibsUpload = multer({
  storage: storage,
  fileFilter: olibsFileFilter,
});
