import * as xlsx from "xlsx";
import * as fs from "fs";

/**
 * Converts an XLSX file to JSON and saves it to the specified destination path.
 *
 * @param {string} dataDestinationPath - The path to the XLSX file to convert.
 * @param {string} jsonDestinationPath - The path to save the resulting JSON file.
 * @return {void} This function does not return anything.
 */

export const xlsxToJson = (dataDestinationPath, jsonDestinationPath) => {
  try {
    const fileData = fs.readFileSync(dataDestinationPath);
    const workbook = xlsx.read(fileData, { type: "buffer" });
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const jsonData = xlsx.utils.sheet_to_json(sheet);

    // Modify JSON data keys and trim values
    const modifiedJsonData = jsonData.map((item) => {
      const modifiedItem = {};
      for (const key in item) {
        const modifiedKey = key.toLowerCase().replace(/\s+/g, "");
        const modifiedValue = String(item[key]).trim();
        modifiedItem[modifiedKey] = modifiedValue;
      }
      return modifiedItem;
    });

    fs.writeFileSync(
      jsonDestinationPath,
      JSON.stringify(modifiedJsonData, null, 2)
    );
    console.log("JSON file saved to", jsonDestinationPath);
  } catch (error) {
    console.error(error);
    throw new Error("Error converting XLSX to JSON");
  }
};
