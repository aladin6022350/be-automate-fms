/**
 * Creates a schema object with the given type and values.
 *
 * @param {type} type - The type of the schema.
 * @param {type} values - The values of the schema.
 * @return {Object} The created schema object.
 */
export const createSchema = (type, values) => {
  let schema = {
    type: type,
    values: values,
  };

  return schema;
};
