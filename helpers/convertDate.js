/**
 * Converts a serial date to a formatted date string.
 *
 * @param {number} serialDate - The serial date to be converted.
 * @return {string} The formatted date string in the format DD/MM/YYYY.
 */

export const convertDate = (serialDate) => {
  const excelEpoch = new Date(1900, 0, 1); // Excel's epoch date

  const millisecondsPerDay = 24 * 60 * 60 * 1000; // Number of milliseconds in a day
  const dateValue = new Date(
    excelEpoch.getTime() + (serialDate - 1) * millisecondsPerDay
  );

  const day = dateValue.getDate().toString().padStart(2, "0"); // Get day and pad with zero if needed
  const month = (dateValue.getMonth() + 1).toString().padStart(2, "0"); // Get month (add 1 as month is zero-based) and pad with zero if needed
  const year = dateValue.getFullYear();

  return `${day}/${month}/${year}`; // Output: 20/11/2023 (formatted date as DD/MM/YYYY)
};
