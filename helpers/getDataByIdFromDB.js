import mysql from "mysql2";

/**
 * Retrieves a record from the specified table in the database based on the provided ID.
 *
 * @param {object} connectionInfo - The information needed to establish a connection to the database.
 * @param {string} tableName - The name of the table in which to search for the record.
 * @param {number} id - The ID of the record to retrieve.
 * @return {Promise<object|null>} A promise that resolves with the found record if it exists, or null if no record is found.
 */

export const getDataByIdFromDB = (connectionInfo, tableName, id) => {
  return new Promise((resolve, reject) => {
    const connection = mysql.createConnection(connectionInfo);

    connection.connect((err) => {
      if (err) {
        reject(err);
        return;
      }

      const query = `SELECT * FROM ${tableName} WHERE id = ?`;
      connection.query(query, [id], (err, results) => {
        connection.end(); // Close the connection

        if (err) {
          reject(err);
          return;
        }

        // Assuming id is unique, results should contain only one record
        if (results.length === 1) {
          resolve(results[0]); // Resolve with the found record
        } else {
          resolve(null); // Resolve with null if no record found
        }
      });
    });
  });
};

// getRecordById(connectionInfo, tableName, recordId)
//   .then((record) => {
//     if (record) {
//       console.log("Record found:", record);
//     } else {
//       console.log("No record found for the given ID.");
//     }
//   })
//   .catch((error) => {
//     console.error("Error:", error);
//   });
