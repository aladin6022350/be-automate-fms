import * as fs from "fs";

/**
 * Saves an XLSX file from the specified uploaded file path to the destination path.
 *
 * @param {string} uploadedFilePath - The path of the uploaded file.
 * @param {string} destinationPath - The path where the XLSX file will be saved.
 * @return {void}
 */

export const saveXlsxFile = (uploadedFilePath, destinationPath) => {
  fs.copyFile(uploadedFilePath, destinationPath, (err) => {
    if (err) {
      console.error("Error saving the XLSX file:", err);
      return;
    } else {
      console.log("XLSX file saved to", destinationPath);
      return;
    }
  });
};
