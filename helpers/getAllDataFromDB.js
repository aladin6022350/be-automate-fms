import mysql from "mysql2";

/**
 * Retrieves all data from a specified table in the database.
 *
 * @param {object} connectionInfo - The connection information for the database.
 * @param {string} tableName - The name of the table from which data will be retrieved.
 * @param {function} callback - A callback function to handle the result or error.
 */
export const getAllDataFromDB = (connectionInfo, tableName, callback) => {
  const connection = mysql.createConnection(connectionInfo);

  connection.connect((err) => {
    if (err) {
      callback(err, null);
      return;
    }

    const selectQuery = `SELECT * FROM ${tableName}`;

    connection.query(selectQuery, (error, results) => {
      connection.end(); // Close the connection

      if (error) {
        callback(error, null);
        return;
      }

      callback(null, results);
    });
  });
};

// getAllDataFromDB(connectionInfo, tableName, (error, result) => {
//   if (error) {
//     console.error("Error retrieving data:", error);
//     return;
//   }

//   console.log("Retrieved data:", result);
//   // Handle the retrieved data as needed
// });
