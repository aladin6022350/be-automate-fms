import mysql from "mysql2";

/**
 * Deletes records from the specified table in the database based on the given condition.
 *
 * @param {Object} connectionInfo - The information required to establish a connection to the database.
 * @param {string} tableName - The name of the table from which records should be deleted.
 * @param {string} condition - The condition that determines which records should be deleted.
 * @param {function} callback - The callback function to be called upon completion of the deletion operation.
 * @return {void}
 */

export const deleteFromDB = (
  connectionInfo,
  tableName,
  condition,
  callback
) => {
  const connection = mysql.createConnection(connectionInfo);

  connection.connect((err) => {
    if (err) {
      callback(err, null);
      return;
    }

    const deleteQuery = `DELETE FROM ${tableName} WHERE ${condition}`;

    connection.query(deleteQuery, (error, results) => {
      connection.end(); // Close the connection

      if (error) {
        callback(error, null);
        return;
      }

      callback(null, results);
    });
  });
};

// deleteFromDB(connectionInfo, tableName, condition, (error, result) => {
//     if (error) {
//       console.error('Error deleting data:', error);
//       return;
//     }

//     console.log('Data deleted successfully:', result);
//   });
