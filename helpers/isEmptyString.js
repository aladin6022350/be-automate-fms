/**
 * Check if a given string is empty.
 *
 * @param {string} str - The string to be checked.
 * @return {boolean} Returns true if the string is empty, false otherwise.
 */

export const isEmptyString = (str) => {
  if (str === null || str === undefined || str === "".trim()) {
    return true;
  }
  return false;
};
