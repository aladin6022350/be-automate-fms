/**
 * Returns a timestamp in the 'YYYY-MM-DDTHH:MM:SS.sssZ' format adjusted to UTC+7 (WIB).
 *
 * @return {string} The formatted timestamp in UTC+7 (WIB).
 */

export const getTimestampInWIB = () => {
  const date = new Date();
  // Adjust the timezone to UTC+7 (WIB)
  date.setUTCHours(date.getUTCHours() + 7);

  // Format the timestamp in the 'YYYY-MM-DDTHH:MM:SS.sssZ' format
  const timestamp = date.toISOString().slice(0, 23) + "Z"; // Include milliseconds and 'Z' for UTC

  return timestamp;
};
