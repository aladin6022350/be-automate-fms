import { convertDate } from "../../helpers/convertDate";
import { getTimestampInWIB } from "../../helpers/getTimeStamp";
import "cypress-if";

// type delay
Cypress.Commands.overwrite(
  "type",
  (originalFn, subject, text, options = {}) => {
    options.delay = 0;

    return originalFn(subject, text, options);
  }
);

describe("Automate Olibs", () => {
  const data = Cypress.env("data");
  const config = Cypress.env("config");
  const username = Cypress.env("OLIBS_USERNAME");
  const password = Cypress.env("OLIBS_PASSWORD");
  const confirmationUsername = Cypress.env("CONFIRMATION_USERNAME");
  const confirmationPassword = Cypress.env("CONFIRMATION_PASSWORD");
  const url = Cypress.env("OLIBS_URL");

  beforeEach(() => {
    cy.fixture(data).as("data");
    cy.fixture(config).then((configData) => {
      configData.sort((a, b) => a.queue - b.queue);
      cy.wrap(configData).as("sortedConfig");
    });

    cy.visit(url);
    cy.get(".z-textbox").eq(0).type(username);
    cy.get(".z-textbox").eq(1).type(password);

    cy.get(".z-button-cm").eq(0).click();

    // Force Login
    cy.get(".z-label")
      .eq(4)
      .contains("Sudah Login")
      .if()
      .then(() => {
        cy.get(".z-button-cm").eq(2).click();
      })
      .else()
      .then(() => {
        cy.task("log", "No one logged");
      });
  });

  it("Automate Olibs", () => {
    cy.get("@data").each((data, index) => {
      cy.task("log", `Iteration ${index + 1} start...`);
      cy.get("@sortedConfig").each((config) => {
        let steps = config.steps;
        steps.forEach((step) => {
          let action = step.action.trim().toLowerCase();
          let getField = step.field;
          let field = getField
            ? getField.trim().toLowerCase().replace(/ /g, "")
            : "";

          if (action !== "options") {
            cy.task(
              "log",

              `FIELD : ${field}\n${getTimestampInWIB()}\nIteration : ${
                index + 1
              }\n{Phase : ${config.description}, Step : ${
                step.description
              }, Action : ${action}, Target : ${step.target}, Position : ${
                step.pos
              }}`
            );
          }

          switch (action) {
            case "click":
              {
                step.waitBefore ? cy.wait(step.waitBefore) : "";
              }
              cy.get(step.target)
                .if("have.length.at.least", step.pos)
                .then(() => {
                  cy.get(step.target)
                    .eq(step.pos)
                    .scrollIntoView()
                    .should("be.visible")
                    .click();
                })
                .else()
                .then(() => {
                  cy.task(
                    "log",
                    "ELEMENT IS NOWHERE TO BE FOUND! SKIPPING TO THE NEXT ELEMENT..."
                  );
                });

              {
                step.waitAfter ? cy.wait(step.waitAfter) : "";
              }

              break;

            case "dblclick":
              {
                step.waitBefore ? cy.wait(step.waitBefore) : "";
              }

              cy.get(step.target)
                .eq(step.pos)
                .scrollIntoView()
                .should("be.visible")
                .dblclick();

              {
                step.waitAfter ? cy.wait(step.waitAfter) : "";
              }

              break;

            case "type": {
              {
                step.waitBefore ? cy.wait(step.waitBefore) : "";
              }

              if (step.optional) {
                if (data[field]) {
                  if (step.convert) {
                    cy.get(step.target)
                      .eq(step.pos)
                      .scrollIntoView()
                      .should("be.visible")
                      .clear()
                      .type(`${convertDate(data[field])}{enter}`)
                      .blur();
                    {
                      step.waitAfter ? cy.wait(step.waitAfter) : "";
                    }
                    break;
                  }
                  cy.get(step.target)
                    .eq(step.pos)
                    .scrollIntoView()
                    .should("be.visible")
                    .clear()
                    .type(`${data[field]}{enter}`)
                    .blur();

                  {
                    step.waitAfter ? cy.wait(step.waitAfter) : "";
                  }
                  break;
                }
                cy.task(
                  "log",
                  `Field "${getField}" HAS NO DATA! SKIPPING TO THE NEXT ELEMENT...`
                );
                break;
              }

              if (step.convert) {
                cy.get(step.target)
                  .eq(step.pos)
                  .scrollIntoView()
                  .should("be.visible")
                  .clear()
                  .type(`${convertDate(data[field])}{enter}`)
                  .blur();

                {
                  step.waitAfter ? cy.wait(step.waitAfter) : "";
                }
                break;
              }

              if (step.confirmation) {
                cy.get(step.target)
                  .if("have.length.at.least", step.pos)
                  .then(() => {
                    let confirmation = step.confirmation
                      .trim()
                      .toLowerCase()
                      .replace(/ /g, "");

                    cy.get(step.target)
                      .eq(step.pos)
                      .scrollIntoView()
                      .should("be.visible")
                      .clear()
                      .type(
                        confirmation === "username"
                          ? confirmationUsername
                          : confirmationPassword
                      )
                      .blur();

                    {
                      step.waitAfter ? cy.wait(step.waitAfter) : "";
                    }
                  })
                  .else()
                  .then(() => {
                    cy.task(
                      "log",
                      "ELEMENT IS NOWHERE TO BE FOUND! SKIPPING TO THE NEXT ELEMENT..."
                    );
                  });

                break;
              }

              cy.get(step.target)
                .eq(step.pos)
                .scrollIntoView()
                .should("be.visible")
                .if("not.disabled")
                .then(() => {
                  cy.get(step.target)
                    .eq(step.pos)
                    .clear()
                    .type(`${data[field]}{enter}`)
                    .blur();

                  {
                    step.waitAfter ? cy.wait(step.waitAfter) : "";
                  }
                })
                .else()
                .then(() => {
                  cy.task(
                    "log",
                    "ELEMENT IS DISABLED! SKIPPING TO THE NEXT ELEMENT..."
                  );
                });

              break;
            }

            case "options":
              {
                step.waitBefore ? cy.wait(step.waitBefore) : "";
              }

              if (step.optional) {
                if (data[field]) {
                  let filteredOptions = step.options.filter(
                    (option) =>
                      option.value.trim().toLowerCase().replace(/ /g, "") ===
                      data[field].trim().toLowerCase().replace(/ /g, "")
                  );

                  let target = filteredOptions[0].target;
                  let position = filteredOptions[0].pos;

                  cy.get(target)
                    .eq(position)
                    .scrollIntoView()
                    .should("be.visible")
                    .click();

                  {
                    step.waitAfter ? cy.wait(step.waitAfter) : "";
                  }
                  break;
                }

                break;
              }

              let filteredOptions = step.options.filter(
                (option) =>
                  option.value.trim().toLowerCase().replace(/ /g, "") ===
                  data[field].trim().toLowerCase().replace(/ /g, "")
              );

              if (filteredOptions.length > 0) {
                let target = filteredOptions[0].target;
                let position = filteredOptions[0].pos;
                let value = filteredOptions[0].value;

                cy.task(
                  "log",
                  `${getTimestampInWIB()}\nIteration : ${index + 1}\n{Phase : ${
                    config.description
                  }, Step : ${
                    step.description
                  }, Action : ${action}, Target : ${target}, Position : ${position}, Value: ${value}}`
                );

                cy.get(target)
                  .eq(position)
                  .scrollIntoView()
                  .should("be.visible")
                  .click();

                {
                  step.waitAfter ? cy.wait(step.waitAfter) : "";
                }

                break;
              }

            default:
              break;
          }
        });
      });

      cy.task("log", `Iteration ${index + 1} end...`);

      cy.reload();
    });
  });
});
