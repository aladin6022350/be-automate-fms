import express from "express";
import { configDotenv } from "dotenv";
import cors from "cors";
import {
  automate,
  logToDB,
  genericScrape,
  downloadXLSX,
  automateOlibs,
  getAllConfigs,
  postConfig,
  deleteConfig,
  getConfigById,
} from "./controllers/controllers.js";
import { configUpload, genericUpload, olibsUpload } from "./helpers/multer.js";

configDotenv({ path: "env/.env" });

const app = express();

app.use(express.json());
app.use(cors());

app.get("/get_config/:id", getConfigById);
app.delete("/delete_config/:id", deleteConfig);
app.post("/post_config", configUpload.single("file"), postConfig);
app.get("/get_configs", getAllConfigs);

app.post("/automate_olibs", olibsUpload.single("xlsx"), automateOlibs);

app.post("/scrape", genericScrape);
app.post("/automate", genericUpload.single("file"), automate);

app.post("/log", logToDB);
app.get("/download/:name", downloadXLSX);

app.listen(process.env.PORT, () => {
  console.log(`LISTENING ON PORT ${process.env.PORT}`);
});
